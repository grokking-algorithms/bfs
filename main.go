package main

import (
	"fmt"
	"github.com/gammazero/deque"
)

type Person struct {
	id      int
	name    string
	friends []Person
}

func search(root int, graph map[int]Person, finder func(p Person) bool) *Person {
	var searchQueue deque.Deque
	searchQueue.PushBack(root)
	searched := make(map[int]Person)

	var id int
	for searchQueue.Len() != 0 {
		id = searchQueue.PopFront().(int)
		if _, alreadySearched := searched[id]; alreadySearched {
			continue
		}

		person := graph[id]
		isSearched := finder(person)

		if isSearched {
			return &person
		} else {
			for _, v := range person.friends {
				searchQueue.PushBack(v.id)
			}
			searched[person.id] = person
		}
	}

	return nil
}

func main() {
	peggy := Person{
		id:      5,
		name:    "peggy",
		friends: nil,
	}
	alice := Person{
		id:      2,
		name:    "alice",
		friends: []Person{peggy},
	}
	anuj := Person{
		id:      6,
		name:    "anuj",
		friends: nil,
	}
	bob := Person{
		id:      3,
		name:    "bob",
		friends: []Person{anuj, peggy},
	}
	thom := Person{
		id:      7,
		name:    "thom",
		friends: nil,
	}
	jonny := Person{
		id:      8,
		name:    "jonny",
		friends: nil,
	}
	claire := Person{
		id:      4,
		name:    "claire",
		friends: []Person{thom, jonny},
	}
	you := Person{
		id:      1,
		name:    "you",
		friends: []Person{alice, bob, claire},
	}

	searchFunc := func(p Person) bool {
		return len(p.name) == 4
	}

	graph := make(map[int]Person)
	graph[1] = you
	graph[2] = alice
	graph[3] = bob
	graph[4] = claire
	graph[5] = peggy
	graph[6] = anuj
	graph[7] = thom
	graph[8] = jonny

	p := search(1, graph, searchFunc)

	if p != nil {
		fmt.Printf("Searched person is: %s", p.name)
	} else {
		fmt.Printf("Person not found")
	}
}
